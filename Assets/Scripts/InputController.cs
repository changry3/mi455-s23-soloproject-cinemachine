using UnityEngine;
using UnityEngine.InputSystem;

public class InputController : MonoBehaviour
{
    public TankControl tankControl;
    public GunControl gunControl;

    // private void Start()
    // {
    //     tankControl.currentMovement = new(1, 1);
    // }

    public void OnMovement(InputAction.CallbackContext context)
    {
        Vector2 movement = context.ReadValue<Vector2>();
        tankControl.currentMovement = new Vector2(movement.y, movement.x);
    }

    public void OnTurret(InputAction.CallbackContext context)
    {
        Vector2 movement = context.ReadValue<Vector2>();
        tankControl.turret.currentMovement = movement;
    }

    public void Fire(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            gunControl.DoFire();
        }
    }

    public void NextCamera(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            CameraManager.singleton.NextCamera();
        }
    }

    public void PrevCamera(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            CameraManager.singleton.PrevCamera();
        }
    }

    public void ToggleFollowShell(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            SettingsManager.singleton.followShellEnabled =
                !SettingsManager.singleton.followShellEnabled;
        }
    }
}