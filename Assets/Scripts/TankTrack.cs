using UnityEngine;
using Cinemachine;
using System.Linq;

public class TankTrack : MonoBehaviour
{
    [Header("Autogenereted values")]
    public WheelCollider[] wheels;

    public CinemachineImpulseSource engineImpulse;

    private Scroll_Track scroll;

    private void Start()
    {
        wheels = GetComponentsInChildren<WheelCollider>();
        gameObject.RequireComponentAuto(out engineImpulse, "impulse source");
        scroll = gameObject.AddComponentIfMissing<Scroll_Track>();
    }

    public void Drive(float driveForce, float rollingResistance, float maxSpeed,
        float speedCorrection)
    {
        foreach (var wheel in wheels)
        {
            float angularV = maxSpeed / wheel.radius;
            float rpm = angularV * (30 / Mathf.PI);

            //print($"{rpm}, {wheel.rpm}");

            if (rpm > wheel.rpm)
            {
                wheel.motorTorque = driveForce;
                wheel.brakeTorque = driveForce == 0 ? rollingResistance : 0;
            }
            else
            {
                wheel.motorTorque = 0;
                wheel.brakeTorque = speedCorrection;
            }
        }
    }

    private void FixedUpdate()
    {
        float avgRPM = wheels.Average(w => w.rpm);
        float avgAngularV = avgRPM * (Mathf.PI / 30);
        float avgLinV = avgAngularV / wheels.Average(w => w.radius);

        scroll.scrollSpeed = avgLinV * -0.001f;

        engineImpulse.GenerateImpulseAt(transform.root.position,
            engineImpulse.m_DefaultVelocity * avgLinV);
    }
}