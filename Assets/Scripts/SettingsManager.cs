﻿using System.Collections;
using UnityEngine;

public class SettingsManager : MonoBehaviour
{
    public static SettingsManager singleton;

    public bool followShellEnabled = true;

    private void Start()
    {
        singleton = this;
    }
}