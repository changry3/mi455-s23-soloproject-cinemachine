using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IgnoreColliders : MonoBehaviour
{
    public List<Collider> ignored;

    private void Start()
    {
        var selfCollider = GetComponent<Collider>();
        foreach (var ignore in ignored)
        {
            Physics.IgnoreCollision(ignore, selfCollider);
        }
    }

    public void InitializeIgnores(IEnumerable<Collider> colliders)
    {
        ignored = new(colliders);
    }
}
