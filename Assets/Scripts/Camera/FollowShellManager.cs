﻿using Cinemachine;
using System.Collections;
using UnityEngine;

public class FollowShellManager : MonoBehaviour
{
    [System.Serializable]
    public struct VirtualCameraConfig
    {
        public CinemachineVirtualCamera camera;

        [Tooltip("New timescale for the portion.")]
        public float timeScale;

        [Tooltip("How long to wait for the portion.")]
        public float delay;

        [Tooltip("If true, then only set the camera to look at the shell. " +
            "Follow will remain unset.")]
        public bool lookAtOnly;

        public void SetActive(bool active)
        {
            camera.gameObject.SetActive(active);
        }

        public void Configure(Shell shell)
        {
            SetActive(true);
            camera.LookAt = shell.transform;

            if (!lookAtOnly)
            {
                camera.Follow = shell.transform;
            }
        }
    }

    [Header("User settings")]
    public VirtualCameraConfig[] justFiredCameras;

    public CinemachineClearShot landedCamera;

    public static FollowShellManager singleton;

    private Shell shellInFlight;

    private void Start()
    {
        singleton = this;
        DisableAllCameras();
    }

    public void DoLaunched(Shell shell)
    {
        CameraManager.singleton.DisableAll();

        shellInFlight = shell;
        var selectedCam = justFiredCameras.GetRandomValue();

        float timeScale = selectedCam.timeScale;

        if (timeScale <= 0)
            timeScale = 1;
        else
            timeScale /= shell.initialVelocity;

        float waitTime = selectedCam.delay;

        selectedCam.Configure(shell);

        TimeScaleManager.singleton.AddTimeScale(shell.GetInstanceID(),
            timeScale, true);

        StartCoroutine(EndLaunch(shell.GetInstanceID(), waitTime));
    }

    public void ShellLanded(Shell shell, RaycastHit hit)
    {
        if (shell == shellInFlight)
        {
            GameObject lookTarget = new("LookTarget");
            lookTarget.AddComponent<DestroyAfter>().destroyAfter = 10f;
            lookTarget.transform.position = hit.transform.position;

            landedCamera.gameObject.SetActive(true);
            landedCamera.LookAt = lookTarget.transform;
            landedCamera.Follow = lookTarget.transform;
        }
    }

    public void EndFollow(Shell shell)
    {
        if (shell == shellInFlight)
        {
            shellInFlight = null;
            StartCoroutine(EndLanded(shell.GetInstanceID(), 1));
        }
    }

    private IEnumerator EndLaunch(int id, float delay)
    {
        yield return new WaitForSecondsRealtime(delay);

        DisableAllCameras();
        BeginFlight();
    }

    private IEnumerator EndLanded(int id, float delay)
    {
        yield return new WaitForSecondsRealtime(delay);

        //TimeScaleManager.singleton.RemoveTimeScale(id, true);
        CameraManager.singleton.CurrentSolo();
        DisableAllCameras();
    }

    private void BeginFlight()
    {
        if (shellInFlight)
            shellInFlight.flightCameraControl.BeginFlight(shellInFlight);
    }

    private void DisableAllCameras()
    {
        foreach (var cam in justFiredCameras)
        {
            cam.SetActive(false);
        }

        landedCamera.gameObject.SetActive(false);
    }
}