﻿using System.Collections;
using UnityEngine;

public class ToggleActive : MonoBehaviour
{
    public GameObject target;

    public void Toggle()
    {
        target.SetActive(!target.activeInHierarchy);
    }
}