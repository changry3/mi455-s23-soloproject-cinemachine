﻿using System.Collections;
using UnityEngine;

public class DestroyAfter : MonoBehaviour
{
    public float destroyAfter = 0;

    private void Start()
    {
        Destroy(gameObject, destroyAfter);
    }
}