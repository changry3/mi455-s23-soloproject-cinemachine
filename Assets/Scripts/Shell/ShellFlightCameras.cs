﻿using Cinemachine;
using System.Collections;
using UnityEngine;
public class ShellFlightCameras : MonoBehaviour
{
    [System.Serializable]
    public struct VirtualCameraConfig
    {
        public CinemachineVirtualCamera camera;

        [Tooltip("New timescale for the portion.")]
        public float timeScale;

        public void SetActive(bool active)
        {
            camera.gameObject.SetActive(active);
        }

        public void Configure(Shell shell)
        {
            SetActive(true);
        }
    }

    public VirtualCameraConfig[] flightCameras;

    public Transform cameraHolder;

    private VirtualCameraConfig selectedCamera;

    private void Start()
    {
        DisableAllCameras();
    }

    public void BeginFlight(Shell shell)
    {
        selectedCamera = flightCameras.GetRandomValue();

        float timeScale = selectedCamera.timeScale;

        if (timeScale <= 0)
            timeScale = 1;
        else
            timeScale /= shell.initialVelocity;

        selectedCamera.Configure(shell);
        TimeScaleManager.singleton.ReplaceTimeScale(
            shell.GetInstanceID(), timeScale, false);
    }

    public void DisableAllCameras()
    {
        foreach (var cam in flightCameras)
        {
            cam.SetActive(false);
        }
    }
}