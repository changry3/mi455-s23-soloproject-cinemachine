﻿using Cinemachine;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class VCamTransitionEffector : MonoBehaviour
{
    public enum IncomingMode
    {
        Disabled, IfFrom, IfNotFrom
    }

    public enum OutgoingMode
    {
        Disabled, IfTo, IfNotTo
    }
    public CinemachineVirtualCamera selfCamera;

    [Tooltip("Determines compairisons based on camera transisions heading in.")]
    public IncomingMode incoming;

    [Tooltip("Determines compairisons based on camera transisions heading out.")]
    public OutgoingMode outgoing;

    public CinemachineVirtualCamera[] compairisons;

    public UnityEvent transistionEvent;

    public void CameraTransition(ICinemachineCamera newCamera, ICinemachineCamera oldCamera)
    {
        bool doInvoke = false;
        if (newCamera.Equals(selfCamera))
        {
            switch (incoming)
            {
                case IncomingMode.IfFrom:
                    doInvoke = compairisons.Contains(oldCamera);
                    break;
                case IncomingMode.IfNotFrom:
                    doInvoke = !compairisons.Contains(oldCamera);
                    break;
            }
        }
        else if (oldCamera != null && oldCamera.Equals(selfCamera))
        {
            switch (outgoing)
            {
                case OutgoingMode.IfTo:
                    doInvoke = compairisons.Contains(newCamera);
                    break;
                case OutgoingMode.IfNotTo:
                    doInvoke = !compairisons.Contains(newCamera);
                    break;
            }
        }

        if (doInvoke)
        {
            transistionEvent.Invoke();
        }
    }
}