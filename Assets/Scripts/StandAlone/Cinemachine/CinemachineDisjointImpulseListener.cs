﻿using Cinemachine;
using System.Collections;
using UnityEngine;

/// <summary>
/// I just love verbosity.
/// Same as CinemachineIndependentImpulseListener,
/// with the additional ability to listen at a different
/// location.
/// </summary>
public class CinemachineDisjointImpulseListener : MonoBehaviour
{
    private Vector3 impulsePosLastFrame;
    private Quaternion impulseRotLastFrame;

    /// <summary>
    /// Impulse events on channels not included in the mask will be ignored.
    /// </summary>
    [Tooltip("Impulse events on channels not included in the mask will be ignored.")]
    [CinemachineImpulseChannelProperty]
    public int m_ChannelMask;

    /// <summary>
    /// Gain to apply to the Impulse signal.
    /// </summary>
    [Tooltip("Gain to apply to the Impulse signal.  1 is normal strength.  Setting this to 0 completely mutes the signal.")]
    public float m_Gain;

    /// <summary>
    /// Enable this to perform distance calculation in 2D (ignore Z).
    /// </summary>
    [Tooltip("Enable this to perform distance calculation in 2D (ignore Z)")]
    public bool m_Use2DDistance;

    /// <summary>
    /// Enable this to process all impulse signals in camera space.
    /// </summary>
    [Tooltip("Enable this to process all impulse signals in camera space")]
    public bool m_UseLocalSpace;

    /// <summary>
    /// This controls the secondary reaction of the listener to the incoming impulse.  
    /// The impulse might be for example a sharp shock, and the secondary reaction could
    /// be a vibration whose amplitude and duration is controlled by the size of the 
    /// original impulse.  This allows different listeners to respond in different ways 
    /// to the same impulse signal.
    /// </summary>
    [Tooltip("This controls the secondary reaction of the listener to the incoming impulse.  "
        + "The impulse might be for example a sharp shock, and the secondary reaction could "
        + "be a vibration whose amplitude and duration is controlled by the size of the "
        + "original impulse.  This allows different listeners to respond in different ways "
        + "to the same impulse signal.")]
    public CinemachineImpulseListener.ImpulseReaction m_ReactionSettings;

    /// <summary>
    /// Transform to listen at.
    /// </summary>
    [Tooltip("Transform to listen at.")]
    public Transform listenAt;

    private void Reset()
    {
        m_ChannelMask = 1;
        m_Gain = 1;
        m_Use2DDistance = false;
        m_UseLocalSpace = true;
        m_ReactionSettings = new CinemachineImpulseListener.ImpulseReaction
        {
            m_AmplitudeGain = 1,
            m_FrequencyGain = 1,
            m_Duration = 1f
        };
    }

    private void OnEnable()
    {
        impulsePosLastFrame = Vector3.zero;
        impulseRotLastFrame = Quaternion.identity;
    }

    private void Update()
    {
        // Unapply previous shake
        transform.position -= impulsePosLastFrame;
        transform.rotation = transform.rotation * Quaternion.Inverse(impulseRotLastFrame);
    }

    // We do this in LateUpdate specifically to support attaching this script to the
    // Camera with the CinemachineBrain.  Script execution order is after the brain.
    private void LateUpdate()
    {
        // Apply the shake
        bool haveImpulse = CinemachineImpulseManager.Instance.GetImpulseAt(
            listenAt.position, m_Use2DDistance, m_ChannelMask,
            out impulsePosLastFrame, out impulseRotLastFrame);
        bool haveReaction = m_ReactionSettings.GetReaction(
            Time.deltaTime, impulsePosLastFrame, out var reactionPos, out var reactionRot);

        if (haveImpulse)
        {
            impulseRotLastFrame = Quaternion.SlerpUnclamped(
                Quaternion.identity, impulseRotLastFrame, m_Gain);
            impulsePosLastFrame *= m_Gain;
        }
        if (haveReaction)
        {
            impulsePosLastFrame += reactionPos;
            impulseRotLastFrame *= reactionRot;
        }
        if (haveImpulse || haveReaction)
        {
            if (m_UseLocalSpace)
                impulsePosLastFrame = transform.rotation * impulsePosLastFrame;

            transform.position += impulsePosLastFrame;
            transform.rotation = transform.rotation * impulseRotLastFrame;
        }
    }
}