﻿using System.Collections;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;

public class CutoutMaskImage : Image
{
    public override Material materialForRendering
    {
        get
        {
            var copy = new Material(base.materialForRendering);
            copy.SetInt("_StencilComp", (int)CompareFunction.NotEqual);
            return copy;
        }
    }
}