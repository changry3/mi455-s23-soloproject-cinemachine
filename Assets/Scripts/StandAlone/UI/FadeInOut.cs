﻿using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class FadeInOut : MonoBehaviour
{
    public AnimationCurve fadeCurve;

    public Image[] targets;

    public bool startHidden;

    private float t;
    private int direction;
    private Transform previousParent;

    private void Start()
    {
        targets = GetComponentsInChildren<Image>();

        if (startHidden)
        {
            SetTargetsAlpha(0);
        }
    }

    private void Update()
    {
        float duration = fadeCurve.keys.Max(k => k.time);
        if (direction == 1)
        {
            if (t < duration)
            {
                t += Time.deltaTime;
                SetTargetsAlpha(t);
            }
            else
            {
                t = 0;
                direction = 0;
            }
        }
        else if (direction == -1)
        {
            if (t < duration)
            {
                t += Time.deltaTime;
                SetTargetsAlpha(fadeCurve.Evaluate(duration - t));
            }
            else
            {
                t = 0;
                direction = 0;

                if (previousParent)
                {
                    transform.parent = previousParent;
                    previousParent = null;
                }
            }
        }
    }

    private void SetTargetsAlpha(float alpha)
    {
        foreach (var target in targets)
        {
            Color targetColor = target.color;
            targetColor.a = alpha;
            target.color = targetColor;
        }
    }

    public void FadeIn()
    {
        SetTargetsAlpha(0);

        direction = 1;
        t = 0;
    }

    public void FadeOut(bool asOrphan)
    {
        SetTargetsAlpha(1);

        direction = -1;
        t = 0;

        if (asOrphan)
        {
            previousParent = transform.parent;
            transform.parent = null;
        }
    }
}