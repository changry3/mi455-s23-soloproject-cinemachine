﻿using System.Collections;
using UnityEngine;

public class AmmoDetonationLocation : MonoBehaviour
{
    public float explosionForce = 500;
    public float explosionRadius = 15;
}