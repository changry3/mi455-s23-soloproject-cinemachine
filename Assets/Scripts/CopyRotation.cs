﻿using System.Collections;
using UnityEngine;

public class CopyRotation : MonoBehaviour
{
    public Transform target;

    private void Update()
    {
        transform.rotation = target.rotation;
    }
}